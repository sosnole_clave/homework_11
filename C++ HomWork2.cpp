﻿#include<iostream>
#include<string>

int main()
{
	std::cout << "Enter your line: ";
	std::string line;
		std::getline(std::cin, line);
		std::cout << "Your line: " << line << "\n";
		std::cout << "Length " << line.length() << "\n";
		std::cout << "First symbol " << line[0] << "\n";
		std::cout << "Last symbol " << line[line.size() - 1] << "\n";
		return 0;
}